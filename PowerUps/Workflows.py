from Trello import TrelloCustomFieldDefinition, TrelloCustomField


def custom_field_list_workflow(cf: TrelloCustomField, cf_item: dict) -> str:
    if not cf or not cf.options or not cf_item or "idValue" not in cf_item:
        cf_value = "unknown"
    else:
        cf_value = next((option["value"]["text"] for option in cf.options if option["id"] == cf_item["idValue"]), "")

    return cf_value


def custom_field_list_color_workflow(cf: TrelloCustomField, cf_item: dict) -> str:
    if not cf or not cf.options or not cf_item or "idValue" not in cf_item:
        cf_value = "unknown"
    else:
        cf_value = next((option["color"] for option in cf.options if option["id"] == cf_item["idValue"]), "")

    return cf_value


class CustomFieldListValue(TrelloCustomFieldDefinition):
    def __init__(self, cf_id, name):
        super().__init__(cf_id, name, custom_field_list_workflow)


class CustomFieldListColor(TrelloCustomFieldDefinition):
    def __init__(self, cf_id, name):
        super().__init__(cf_id, name, custom_field_list_color_workflow)
