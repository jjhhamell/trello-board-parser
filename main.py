from GameBoard import CompletedGame


JSON_FILE_PATH = "resources/games.json"


def main():
    game_data = CompletedGame(JSON_FILE_PATH)
    df = game_data.create_basic_data_frame()
    print(df)


if __name__ == '__main__':
    main()
