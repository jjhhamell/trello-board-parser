from __future__ import annotations
from typing import Union, List, Callable

import json


class TrelloCustomFieldDefinition:
    """
    Defines Custom Field from the user.

    Custom fields have diferent options of data, and maybe the user want to retrieve the data
    in diferente ways, here, this let you create a callback function to retrieve it.

    The callback function is associated with the custom field id and a name.

    """
    workflow: TrelloCustomFieldWorkFlow = None
    name: str = None
    id: str = None

    def __init__(self, custom_field_id: str, custom_field_name: str, custom_field_workflow: TrelloCustomFieldWorkFlow = None):
        """
        Constructor of the TrelloCustomFieldDefinition.

        :param custom_field_id: ID of the custom field
        :param custom_field_name: Name of the custom field
        :param custom_field_workflow: Callback function to retrieve the data as the user want
        """
        self.id = custom_field_id
        self.name = custom_field_name
        self.workflow = custom_field_workflow


class TrelloCard:
    """
    Class to define a Card in Trello.

    It lets you retrieve any of its parameter using directly the name on it
    """
    name: str = None
    id: str = None
    idList: str = None
    customFieldItems: List[dict] = None

    _custom_fields: List[dict] = []
    _custom_attrs: List[str] = []

    def __init__(self, card: dict) -> None:
        """
        Constructor of the class.

        :param card: the card in dict type
        """
        for k, v in card.items():
            setattr(self, k, v)  # map every item and value to an attribute in the class

    def __getattr__(self, item):
        """
        This magic method is configure to get the custom field in case you want to access them
        from the class directly.

        :param item: Custom field name set on TrelloCustomFieldDefinition
        :return: The value of the custom field in case the item is one of them, if the custom field
                 not exist, return unknown string, otherwise, it returns the attribute require.
        """
        if item in self._custom_attrs:
            custom_field_declaration = next(
                (cfi for cfi in self._custom_fields if cfi["attr_name"] == item),
                None
            )
            custom_field_definition: TrelloCustomField = custom_field_declaration["definition"]
            cf_id = custom_field_definition.id
            cf_item = next((cf for cf in self.customFieldItems if cf["idCustomField"] == cf_id), None)

            if custom_field_declaration["workflow"]:
                attr = custom_field_declaration["workflow"](custom_field_definition, cf_item)
            else:
                if custom_field_definition.type == "list":
                    if cf_item is None:
                        attr = "unknown"
                    else:
                        attr = cf_item["idValue"]
                else:
                    if cf_item is None:
                        attr = "unknown"
                    else:
                        attr = cf_item["value"][custom_field_definition.type]
        else:
            attr = super(TrelloCard, self).__getattribute__(item)
        return attr

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"{self.name} - {self.id} - {self.idList}"

    def set_new_custom_field_attr(self, custom_field: TrelloCustomField, attr_name: str = None,
                                  workflow: TrelloCustomFieldWorkFlow = None) -> None:
        """

        :param custom_field:
        :param attr_name:
        :param workflow:
        :return:
        """
        self._custom_fields.append({
            "definition": custom_field,
            "attr_name": attr_name,
            "workflow": workflow
        })
        self._custom_attrs.append(attr_name)

    def has_custom_field(self) -> bool:
        return hasattr(self, "customFieldItems")

    def get_custom_field_ids(self) -> List[str]:
        return [cf["idCustomField"] for cf in self.customFieldItems]


class TrelloList:
    name: str = None
    id: str = None

    def __init__(self, trello_list: dict) -> None:
        for k, v in trello_list.items():
            setattr(self, k, v)


class TrelloCustomField:
    id: str = None
    name: str = None
    type: str = None
    options: list = None

    def __init__(self, c_field: dict) -> None:
        for k, v in c_field.items():
            setattr(self, k, v)


TrelloCustomFieldWorkFlow = Callable[[TrelloCustomField, dict], str]


class TrelloCustomFields:
    def __init__(self, c_fields: list) -> None:
        self._c_fields = [TrelloCustomField(c_field) for c_field in c_fields]

    def __getitem__(self, item: int) -> TrelloCustomField:
        return self._c_fields[item]

    def __len__(self):
        return len(self._c_fields)

    def __str__(self):
        return str(self._c_fields)

    def __iter__(self):
        return iter(self._c_fields)


class TrelloLists:
    def __init__(self, lists: Union[list, filter]) -> None:
        if type(lists) is filter:
            self._lists = list(lists)
        else:
            self._lists = [TrelloList(l) for l in lists]

    def __getitem__(self, item: int) -> TrelloList:
        return self._lists[item]

    def __len__(self):
        return len(self._lists)

    def __str__(self):
        return str(self._lists)

    def __iter__(self):
        return iter(self._lists)

    def __contains__(self, item: str) -> bool:
        return len([l for l in self._lists if l.name == item]) > 0

    def get_list_by_id(self, list_id: str) -> TrelloList:
        return next((l for l in self._lists if l.id == list_id), None)

    def get_list_name_by_id(self, list_id: str) -> str:
        return self.get_list_by_id(list_id).name

    def starts_with(self, text: str) -> TrelloLists:
        return TrelloLists(filter(lambda l: l.name.startswith(text), self._lists))


class TrelloCards:
    def __init__(self, cards: Union[list, filter]) -> None:
        if type(cards) is filter:
            self._cards = list(cards)
        else:
            self._cards = [TrelloCard(card) for card in cards]

    def __getitem__(self, item: int) -> TrelloCard:
        return self._cards[item]

    def __len__(self):
        return len(self._cards)

    def __str__(self):
        return str(self._cards)

    def __iter__(self):
        return iter(self._cards)

    def __contains__(self, item: str) -> bool:
        return len([card for card in self._cards if card.name == item]) > 0

    def starts_with(self, text: str) -> TrelloCards:
        return TrelloCards(filter(lambda l: l.name.startswith(text), self._cards))

    def get_cards_from_list(self, list_id: str):
        return [card for card in self._cards if card.idList == list_id]

    def get_names(self):
        return [card.name for card in self._cards]


class TrelloBoard:
    cards: TrelloCards = None
    lists: TrelloLists = None
    customFields: TrelloCustomFields = None

    board_types = {
        "cards": TrelloCards,
        "lists": TrelloLists,
        "customFields": TrelloCustomFields
    }

    def __init__(self, path: str, custom_fields_workflows: List[TrelloCustomFieldDefinition] = None) -> None:
        """

        :param path:
        :param custom_fields_workflow: by pass through custom fields by creating its own attribute inside trello card
        """
        with open(path) as json_file:
            board = json.load(json_file)
            for k, v in board.items():
                if k in self.board_types:
                    if k == "customFields":
                        setattr(self, k, self.board_types[k](v))
                    else:
                        setattr(self, k, self.board_types[k](v))
                else:
                    setattr(self, k, v)

        if custom_fields_workflows:
            self._set_custom_fields_workflow_to_cards(custom_fields_workflows)

    def _set_custom_fields_workflow_to_cards(self, custom_fields_workflows: List[TrelloCustomFieldDefinition]):
        cards = TrelloCards(filter(lambda card: card.has_custom_field, self.cards))
        for card in cards:
            for cf_id in card.get_custom_field_ids():
                cf: TrelloCustomField = next(cf for cf in self.customFields if cf.id == cf_id)
                wf: TrelloCustomFieldDefinition = next((wf for wf in custom_fields_workflows if wf.id == cf_id), None)
                card.set_new_custom_field_attr(cf, wf.name, wf.workflow)

    def filter_card_by_list_name(self, list_name: str) -> TrelloCards:
        games = self.cards
        list_id = next((l.id for l in self.lists if l.name == list_name), None)

        if list_id is None:
            filter_games = []
        else:
            filter_games = TrelloCards(filter(lambda g: g.idList == list_id, games))

        return filter_games

    def get_list_name_by_id(self, list_id: str):
        return self.lists.get_list_name_by_id(list_id)

    def get_list_by_id(self, list_id: str):
        return self.lists.get_list_by_id(list_id)

    def get_lists(self) -> TrelloLists:
        return self.lists

    def get_list_from_card(self, card: TrelloCard) -> Union[TrelloList, None]:
        return self.get_list_by_id(card.idList)
