import pandas as pd

from Trello import *
from PowerUps import CustomFieldListValue


class NotAFieldException(Exception):
    pass


class CompletedGame:
    def __init__(self, path: str):
        cf_definitions = [
            CustomFieldListValue("5e46629cc5030243b991bc53", "consola"),
            TrelloCustomFieldDefinition("5e465cbf54bb2849248a72e4", "empresa"),
            TrelloCustomFieldDefinition("5e466021ec7b6910cbc0efe2", "nota"),
            TrelloCustomFieldDefinition("5f2940d99ccfa7786e567680", "estudio")
        ]

        self._game_data = TrelloBoard(path, cf_definitions)

    def get_years(self):
        return [int(l.name.split(" ")[-1]) for l in self._game_data.lists.starts_with("Completados")]

    def get_year_by_list(self, trello_list: TrelloList):
        return int(trello_list.name.split(" ")[-1])

    def get_completed_games_by_years(self, years: List[int]) -> List[TrelloCard]:
        cards: List[TrelloCard] = []
        if len(years) > 0:
            for year in years:
                cards += self.get_completed_games_by_year(year)

        return cards

    def get_completed_games_by_year(self, year: int) -> List[TrelloCard]:
        list_name: str = f"Completados {year}"
        if list_name in self._game_data.lists:
            cards = self._game_data.filter_card_by_list_name(list_name)
        else:
            cards = []

        return cards

    def create_basic_data_frame(self) -> pd.DataFrame:
        years = []
        names = []
        marks = []

        completed_cards = self.get_completed_games_by_years(self.get_years())
        for card in completed_cards:
            l = self._game_data.get_list_from_card(card)
            years.append(l.name.split(" ")[-1])
            names.append(card.name)
            marks.append(card.nota)

        return pd.DataFrame({
            "Año": years,
            "Nombre": names,
            "Notas": marks
        })

    def create_data_frame(self, fields: List[str]) -> None:
        """
        Create your own data frame.

        :param fields: Columns of the frame.
        :return:
        """
        pass
